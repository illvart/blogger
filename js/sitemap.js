/*
 * @license
 * Sitemap.js v1.0.0-alpha
 * Copyright (c) 2018-present Muhammad Nur Fuad (illvart).
 * https://illvart.com
 *
 */
(function(win, doc) {
  'use strict';

  var extend =
    Object.assign ||
    function(target, varArgs) {
      var args = arguments;
      if (target == null) {
        throw new TypeError('Cannot convert undefined or null to object');
      }
      var to = Object(target);
      var len = arguments.length;
      for (index = 1; index < len; index++) {
        var obj = args[index];
        if (obj != null) {
          for (var key in obj) {
            if (Object.prototype.hasOwnProperty.call(obj, key)) {
              to[key] = obj[key];
            }
          }
        }
      }
      return to;
    };

  var settings;

  function Sitemap(options) {
    var defaults = {
      url: window.location.href,
      containerId: 'sitemap',
      showNew: 6,
      newText: 'New',
      maxResults: 9999,
      sortAlphabetically: {
        panel: false,
        list: false
      },
      activePanel: 0,
      slideSpeed: {
        down: 300,
        up: 200
      },
      clickCallback: function() {},
      jsonCallback: 'sitemap',
      delay: 0
    };

    var sitemapSettings = extend({}, settings || defaults, options);

    var container = doc.getElementById(sitemapSettings.containerId),
      head = doc.head || doc.getElementsByTagName('head')[0],
      array = [],
      idx,
      idx1,
      idx2,
      idx3;
      
    win[sitemapSettings.jsonCallback] = function(cb) {
    
      for (var title, link, feedEntry = cb.feed.entry, feedCategory = cb.feed.category, content = '', idx4 = 0; feedCategory.length > idx4; ++idx4) {
        array.push(feedCategory[idx4].term);
      }
      
      for (var idx5 = 0; feedEntry.length > idx5; ++idx5) {
        (sitemapSettings.showNew || sitemapSettings.showNew > 0) && idx5 < sitemapSettings.showNew + 1 && (feedEntry[idx5].title.$t += ' %new%');
      }
      
      (feedEntry = sitemapSettings.sortAlphabetically.list
        ? feedEntry.sort(function(a, b) {
            return a.title.$t.localeCompare(b.title.$t);
          })
        : feedEntry),
        sitemapSettings.sortAlphabetically.panel && array.sort();
        
      for (idx = 0; array.length > idx; ++idx) {
        (content += '<h3 class="sitemap-panel">' + array[idx] + '</h3>'), (content += '<div class="sitemap-list"><ol>');
        for (idx1 = 0; feedEntry.length > idx1; ++idx1) {
          title = feedEntry[idx1].title.$t;
          for (idx2 = 0; feedEntry[idx1].link.length > idx2; ++idx2) {
            if ('alternate' == feedEntry[idx1].link[idx2].rel) {
              link = feedEntry[idx1].link[idx2].href;
              break;
            }
          }
          for (idx2 = 0; feedEntry[idx1].category.length > idx2; ++idx2) {
            array[idx] == feedEntry[idx1].category[idx2].term && (content += '<li><a href="' + link + '">' + title.replace(/ \%new\%$/, '') + '</a>' + (title.match(/\%new\%/) ? ' <span class="sitemap-new">' + sitemapSettings.newText + '</span>' : '') + '</li>');
          }
        }
        content += '</ol></div>';
      }

      container.innerHTML = content;

      if (typeof jQuery !== 'undefined') {
        if ($('#' + sitemapSettings.containerId + ' .sitemap-list').hide()) {
          $('#' + sitemapSettings.containerId + ' .sitemap-panel')
            .click(function() {
              $(this).hasClass('active') ||
                (sitemapSettings.clickCallback(this),
                $('#' + sitemapSettings.containerId + ' .sitemap-panel')
                  .removeClass('active')
                  .next()
                  .slideUp(sitemapSettings.slideSpeed.up),
                $(this)
                  .addClass('active')
                  .next()
                  .slideDown(sitemapSettings.slideSpeed.down));
            })
            .eq(sitemapSettings.activePanel - 1)
            .addClass('active')
            .next()
            .slideDown(sitemapSettings.slideSpeed.down);
        }
      }
    };
    
    var script = doc.createElement('script');
    script.async = true;
    script.src = sitemapSettings.url.replace(/\/$/, '') + '/feeds/posts/summary?alt=json-in-script&max-results=' + sitemapSettings.maxResults + '&callback=' + sitemapSettings.jsonCallback;

    win.setTimeout(function() {
      head.appendChild(script);
    }, +sitemapSettings.delay);
  }

  win.Sitemap = Sitemap;
})(window, window.document);